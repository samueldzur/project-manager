from pyexpat import model
from django.db import models
from django.contrib.auth.models import User
from django.forms import CharField

class ArtifactGroup(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)

class Artifact(models.Model):
    id = models.AutoField(primary_key=True)
    artifact_group = models.ForeignKey(ArtifactGroup, on_delete=models.CASCADE)
    access_token = models.CharField(max_length=1024)
    url_to_file = models.CharField(max_length=2048)
    project_id = models.CharField(max_length=100)
    artifact_type = models.CharField(max_length=100)
    file_name = models.CharField(max_length=200)
    file_content = models.TextField()
    gitlab_server = models.CharField(max_length=200)
    branch_name = models.CharField(max_length=200)
    relative_file_path = models.CharField(max_length=1024)
    done = models.BooleanField(default=False)
