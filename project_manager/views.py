from cmath import log
from email.headerregistry import Group
import re
import requests
import base64
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from .models import ArtifactGroup, Artifact

from .forms import CreateUserForm
import markdown
import urllib

def register_page(request):
    if request.user.is_authenticated:
        return redirect('artifact_groups')
    else:
        form = UserCreationForm()

        if request.method == "POST":
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                messages.success(request, "Account was created!")
                return redirect("login")

        context = {'form':form}
        return render(request, 'project_manager/register.html', context)

@login_required(login_url='login')
def create_artifact_group(request):
    context = {}
    if request.method == "POST":
        group = ArtifactGroup(user=request.user, name=request.POST.get("name"))
        group.save()
        return redirect("artifact_groups")
    return render(request, 'project_manager/create_artifact_group.html', context)

def login_page(request):
    if request.user.is_authenticated:
        return redirect('artifact_groups')
    else:
        if request.method == "POST":
            username = request.POST.get("username")
            password = request.POST.get("password")

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('artifact_groups')
            else:
                messages.info(request, "username or password is incorrect")

        context = {}
        return render(request, 'project_manager/login.html', context)

@login_required(login_url='login')
def logout_user(request):
    logout(request)
    return redirect('login')

@login_required(login_url='login')
def artifact_groups(request):
    user_artifact_groups = ArtifactGroup.objects.all().filter(user=request.user)
    if len(user_artifact_groups) > 0:
        user_artifact_groups = user_artifact_groups[::-1]
    print(user_artifact_groups)
    context = {"artifact_groups": user_artifact_groups}
    return render(request, 'project_manager/artifact_groups.html', context)

@login_required(login_url='login')
def artifact_group(request, group_id):
    if ArtifactGroup.objects.filter(id=group_id).exists():
        group = ArtifactGroup.objects.get(id=group_id)
        if group.user != request.user:
            return redirect("artifact_groups")
        requirements = Artifact.objects.filter(artifact_group=group).all().filter(artifact_type="requirements").all()
        umls = Artifact.objects.filter(artifact_group=group).all().filter(artifact_type="uml").all()
        codes = Artifact.objects.filter(artifact_group=group).all().filter(artifact_type="code").all()
        context = {"group": group, "requirements": requirements, "codes": codes, "umls": umls}
        return render(request, 'project_manager/artifact_group.html', context)
    else:
        return redirect("artifact_groups")

def home_page(request):
    if request.user.is_authenticated:
        return redirect('artifact_groups')
    else:
        return render(request, 'project_manager/index.html')

@login_required(login_url='login')
def remove_artifact_group(request, group_id):
    if ArtifactGroup.objects.filter(id=group_id).exists():
        group = ArtifactGroup.objects.get(id=group_id)
        if group.user != request.user:
            return redirect("artifact_groups")
        ArtifactGroup.objects.filter(id=group_id).delete()
    return redirect("artifact_groups")

@login_required(login_url='login')
def add_new_artifact(request, group_id):
    if ArtifactGroup.objects.filter(id=group_id).exists():
        group = ArtifactGroup.objects.get(id=group_id)
        if group.user != request.user:
            return redirect("artifact_groups")
        context = {"group_id": group_id}
        if request.method == "POST":
            url_to_file = request.POST.get("url_to_file")
            artifact_type = request.POST.get("artifact_type")
            project_id = request.POST.get("project_id")
            access_token = request.POST.get("access_token")
            file_name = url_to_file.split("/")[-1]
            gitlab_server = "/".join(url_to_file.split("/")[:3])
            branch_name = ""
            relative_file_path = ""
            if "/-/blob/" in url_to_file:
                splitted_url = url_to_file.split("/-/blob/")[1].split("/")
                branch_name = splitted_url[0]
                if len(splitted_url) > 1:
                    relative_file_path = "/".join(splitted_url[1:])
            artifact = Artifact(artifact_group=group, access_token=access_token, url_to_file=url_to_file, project_id=project_id, artifact_type=artifact_type, file_name=file_name, file_content="", gitlab_server=gitlab_server, branch_name=branch_name, relative_file_path=relative_file_path)
            request_url = gitlab_server + "/api/v4/projects/" + project_id + "/repository/files/" + urllib.parse.quote(relative_file_path, safe="") + "?ref=" + branch_name
            payload = ""
            headers = {'Authorization': 'Bearer ' + access_token}
            try:
                response = requests.request("GET", request_url, headers=headers, data=payload)
                if response.status_code == 200:
                    artifact.file_content = str(base64.b64decode(response.json()["content"]))
                    artifact.save()
                    return redirect('/artifact-group/' + group_id + '/')
                else:
                    context = {"group_id": group_id, "error": "error"}
                    return render(request, 'project_manager/add_new_artifact.html', context)
            except Exception:
                context = {"group_id": group_id, "error": "error"}
                return render(request, 'project_manager/add_new_artifact.html', context)
        return render(request, 'project_manager/add_new_artifact.html', context)
    else:
        return redirect('artifact_groups')

@login_required(login_url='login')
def artifact(request, group_id, artifact_id):
    if ArtifactGroup.objects.filter(id=group_id).exists() and Artifact.objects.filter(id=artifact_id).exists():
        group = ArtifactGroup.objects.get(id=group_id)
        artifact = Artifact.objects.get(id=artifact_id)
        if group.user == request.user and artifact.artifact_group == group:

            request_url = artifact.gitlab_server + "/api/v4/projects/" + artifact.project_id + "/repository/files/" + urllib.parse.quote(artifact.relative_file_path, safe="") + "?ref=" + artifact.branch_name
            payload = ""
            headers = {'Authorization': 'Bearer ' + artifact.access_token}
            try:
                response = requests.request("GET", request_url, headers=headers, data=payload)
                if response.status_code == 200:
                    file_content_from_request = str(base64.b64decode(response.json()["content"]))
                    if artifact.file_content == file_content_from_request:
                        artifact.file_content = file_content_from_request[2:-1]
                        artifact.file_content = artifact.file_content.replace("\\n", "\n")
                        if artifact.file_content.startswith("\\xef\\xbb\\xbf"):
                            artifact.file_content = artifact.file_content[12:]
                        html_markdown = markdown.markdown(artifact.file_content)
                        context = {"group_id": group_id, "artifact_id": artifact_id, "artifact": artifact, "html_markdown": html_markdown}
                        if artifact.artifact_type == "uml":
                            hex_code = ""
                            for i in artifact.file_content:
                                character_hex = hex(ord(i))[2:]
                                if len(character_hex) < 2:
                                    character_hex = "0" + character_hex
                                hex_code += character_hex
                            context["hex_code"] = hex_code
                        return render(request, 'project_manager/artifact.html', context)
                    else:
                        artifact.file_content = file_content_from_request
                        artifact.save()
                        artifact.file_content = file_content_from_request[2:-1]
                        artifact.file_content = artifact.file_content.replace("\\n", "\n")
                        if artifact.file_content.startswith("\\xef\\xbb\\xbf"):
                            artifact.file_content = artifact.file_content[12:]
                        html_markdown = markdown.markdown(artifact.file_content)
                        context = {"group_id": group_id, "artifact_id": artifact_id, "file_changed": "file_changed", "artifact": artifact, "html_markdown": html_markdown}
                        if artifact.artifact_type == "uml":
                            hex_code = ""
                            for i in artifact.file_content:
                                character_hex = hex(ord(i))[2:]
                                if len(character_hex) < 2:
                                    character_hex = "0" + character_hex
                                hex_code += character_hex
                            context["hex_code"] = hex_code
                        return render(request, 'project_manager/artifact.html', context)
                else:
                    context = {"group_id": group_id, "artifact_id": artifact_id, "artifact": artifact, "error": "error"}
                    return render(request, 'project_manager/artifact.html', context)
            except Exception:
                context = {"group_id": group_id, "artifact_id": artifact_id, "artifact": artifact, "error": "error"}
                return render(request, 'project_manager/artifact.html', context)

        else:
            return redirect('artifact_groups')
    else:
        return redirect('artifact_groups')

@login_required(login_url='login')
def remove_artifact(request, group_id, artifact_id):
    if ArtifactGroup.objects.filter(id=group_id).exists() and Artifact.objects.filter(id=artifact_id).exists():
        group = ArtifactGroup.objects.get(id=group_id)
        artifact = Artifact.objects.get(id=artifact_id)
        if group.user == request.user and artifact.artifact_group == group:
            artifact.delete()
            return redirect('/artifact-group/' + group_id)
        else:
            return redirect('artifact_groups')
    else:
        return redirect('artifact_groups')

@login_required(login_url='login')
def toggle_done(request, group_id, artifact_id):
    if ArtifactGroup.objects.filter(id=group_id).exists() and Artifact.objects.filter(id=artifact_id).exists():
        group = ArtifactGroup.objects.get(id=group_id)
        artifact = Artifact.objects.get(id=artifact_id)
        if group.user == request.user and artifact.artifact_group == group:
            artifact.done = not artifact.done
            artifact.save()
            return redirect('/artifact-group/' + group_id)
        else:
            return redirect('artifact_groups')
    else:
        redirect('artifact_groups')