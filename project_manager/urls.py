"""project_manager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.home_page, name="home"),
    path('admin/', admin.site.urls),
    path('register/', views.register_page, name="register"),
    path('login/', views.login_page, name="login"),
    path('logout/', views.logout_user, name="logout"),
    path('artifact-groups/', views.artifact_groups, name="artifact_groups"),
    path('artifact-groups/create/', views.create_artifact_group, name="create_artifact_group"),
    path('artifact-group/<group_id>/', views.artifact_group, name="artifact_group"),
    path('artifact-group/<group_id>/remove/', views.remove_artifact_group, name="remove_artifact_groups"),
    path('artifact-group/<group_id>/add-new-artifact/', views.add_new_artifact, name="add_new_artifact"),
    path('artifact-group/<group_id>/artifact/<artifact_id>/', views.artifact, name="artifact"),
    path('artifact-group/<group_id>/artifact/<artifact_id>/toggle-done/', views.toggle_done, name="toggle_done"),
    path('artifact-group/<group_id>/artifact/<artifact_id>/remove/', views.remove_artifact, name="remove_artifact")
]
